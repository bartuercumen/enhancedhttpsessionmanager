//
//  EnhancedHTTPSessionManager.m
//  
//
//  Created by Bartu Ercümen on 02/01/16.
//  Copyright © 2016 Bartu Ercümen. All rights reserved.
//

#import "EnhancedHTTPSessionManager.h"
#import <Mantle/Mantle.h>

#import "APIManager.h"
#import "APIManager+Authentication.h"


@implementation EnhancedHTTPSessionManager

/**
 * Method description is given on the class definition on header file.
 */
- (NSURLSessionDataTask *)asyncDataTaskWithMethod:(NSString *)method
                                              URL:(NSString *)url
                                       parameters:(id)parameters
                                         assignAs:(Class)targetClass
                                         complete:(nullable void (^)(NSURLSessionDataTask * _Nonnull, NSError * _Nullable, id _Nullable, id _Nullable, NSString * _Nullable))complete
{
    /**
     * Show activity indicator in case the flag is set
     */
    if ([self shouldShowNetworkIndicator:url])
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    
    /**
     * Make the actual request to the AFHTTPSessionManager
     */
    NSURLSessionDataTask *dataTask = [self dataTaskWithHTTPMethod:method URLString:url parameters:parameters uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSString *retrievedToken;
        
        /**
         * Hide activity indicator in case the flag is set
         */
        if ([self shouldShowNetworkIndicator:url])
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        /**
         * Try to parse the token if acquired.
         */
        if ([task.response respondsToSelector:@selector(allHeaderFields)])
        {
            NSDictionary *headers = [task.response performSelector:@selector(allHeaderFields)];
            retrievedToken = [headers objectForKey:@"Authorization"];
            
            if (retrievedToken) {
                retrievedToken = [[retrievedToken componentsSeparatedByString:@" "] objectAtIndex:1];
            }
        }
        
        
        /**
         * Checking for server-side errors.
         */
        if ([responseObject objectForKey:@"error"] != nil)
        {
            NSDictionary *errorResponse = [responseObject objectForKey:@"error"];
        
            /**
             * We have a server-side error.
             * Parse it and send it.
             */
            NSError *error = [NSError errorWithDomain:[self handleErrorMessage:[errorResponse objectForKey:@"message"]]
                                                 code:[[errorResponse objectForKey:@"status_code"] integerValue]
                                             userInfo:errorResponse];
            
            
            return complete(task, error, responseObject, nil, retrievedToken);
        }
        
        /**
         * Check if a target class was provided as a subclass of MTLModel
         * if so, go in for predefined structure
         */
        if ([targetClass isSubclassOfClass:[MTLModel class]])
        {
            /**
             * Actual response (data) is inside a response field
             */
            id actualResponse = [responseObject objectForKey:@"payload"];
            
            if ([actualResponse isKindOfClass:[NSArray class]])
            {
                NSMutableArray *parsed = [NSMutableArray array];
                NSError *MTLParseError;
                
                for (NSDictionary *model in actualResponse)
                {
                    [parsed addObject:[MTLJSONAdapter modelOfClass:[targetClass class] fromJSONDictionary:model error:&MTLParseError]];
                }
                
                /**
                 * MTLJSONAdapter failed to parse the actual response into an object.
                 */
                if (MTLParseError != nil)
                    return complete(task, MTLParseError, responseObject, nil, retrievedToken);
                else
                    return complete(task, nil, responseObject, parsed, retrievedToken);
            }
            else
            {
                NSError *MTLParseError;
                
                id parsed = [MTLJSONAdapter modelOfClass:[targetClass class] fromJSONDictionary:actualResponse error:&MTLParseError];
                
                /**
                 * MTLJSONAdapter failed to parse the actual response into an object.
                 */
                if (MTLParseError != nil)
                    return complete(task, MTLParseError, responseObject, nil, retrievedToken);
                else
                    return complete(task, nil, responseObject, parsed, retrievedToken);
            }
        }
        
        /**
         * We dont have a target class defined, just return the responseObject
         */
        return complete(task, nil, responseObject, nil, retrievedToken);
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error)
    {
        /**
         * Hide activity indicator in case the flag is set
         */
        
        if (self.shouldShowNetworkActivityIndicator)
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSString *result = [[NSString alloc] initWithData:(NSData *)[error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"] encoding:NSUTF8StringEncoding];
        
        NSData *responseData = (NSData *)[error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"];
        
        NSError *errorNew;
        
        if (responseData)
        {
            id responseObject = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
            
            if (responseObject)
            {
                id errorObj = [responseObject objectForKey:@"error"];
                
                if (errorObj)
                {
                    errorNew = [NSError errorWithDomain:[self handleErrorMessage:[errorObj objectForKey:@"message"]]
                                                   code:[[errorObj objectForKey:@"status_code"] integerValue]
                                               userInfo:responseObject];
                }
                else errorNew = [NSError errorWithDomain:@"Beklenmedik bir hata ile karşılaşıldı." code:0 userInfo:nil];
            }
            else errorNew = [NSError errorWithDomain:@"Beklenmedik bir hata ile karşılaşıldı." code:0 userInfo:nil];
        }
        else errorNew = [NSError errorWithDomain:@"Lütfen internet bağlantınızı kontrol edin." code:0 userInfo:nil];
        
        
        complete(task, errorNew, nil, nil, nil);
        
    }];
    
    [dataTask resume];
    
    return dataTask;
}

/**
 * handleErrorMessage undertakes the error message recieved 
 * from the server and simply re-phrases it to show to the end user.
 * 
 * There should be an external error parser that deals with this.
 */
- (NSString *)handleErrorMessage:(NSString *)message
{
    if ([message isEqualToString:@"token_expired"] || [message isEqualToString:@"Token has expired"])
    {
        [[APIManager APIClient] logout];
    }
    else if ([message isEqualToString:@"token_invalid"])
    {
        [[APIManager APIClient] logout];
    }
    else if ([message isEqualToString:@"user_not_found"])
    {
        [[APIManager APIClient] logout];
    }
    else if ([message isEqualToString:@"token_not_provided"])
    {
        return @"Lütfen üyelik girişi yapınız.";
    }
    
    return message;
}

/**
 *
 * Excluding routes for activity network indicator
 *
 */

- (NSArray *)networkActivityIndicatorExcludingRoutes
{
    return @[@"user"];
}

- (BOOL)shouldShowNetworkIndicator:(NSString *)route
{
    return self.shouldShowNetworkActivityIndicator && ![[self networkActivityIndicatorExcludingRoutes] containsObject:route];
}
@end
