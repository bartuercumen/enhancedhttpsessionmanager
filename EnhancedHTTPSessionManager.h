//
//  EnhancedHTTPSessionManager.h
//  
//
//  Created by Bartu Ercümen on 02/01/16.
//  Copyright © 2016 Bartu Ercümen. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

/**
 * This subclasses implements a single method, and acts
 * more like an app specific wrapper.
 *
 * Enhanced keyword for this subclass does not fit that well.
 * It purposes to decrease the amount of code on its own subclasses
 * and clear the process as a singular workhorse within the networking.
 *
 * Further its super class (AFHTTPSessionManager) implementation, it 
 * handles server-side error identification and parsing, Mantle model
 * parsing and Mantle model error parsing, merges two callbacks
 * which are success and failure into a signel complete callback.
 *
 * As a further TODO, this class should handle server error parsing 
 * on a higher level, possibly with an external error parser.
 * 
 * Also, there is a shouldShowNetworkActivityIndicator that simply
 * disables or enables the network activity indicator made by this 
 * method.
 */

@interface EnhancedHTTPSessionManager : AFHTTPSessionManager

@property (nonatomic, assign) BOOL shouldShowNetworkActivityIndicator;

- (nonnull NSURLSessionDataTask *)asyncDataTaskWithMethod:(nonnull NSString *)method
                                                      URL:(nonnull NSString *)url
                                               parameters:(nullable id)parameters
                                                 assignAs:(nullable Class)targetClass
                                                 complete:(nullable void(^)( NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error, _Nullable id responseObject, _Nullable id assigned, NSString * _Nullable token))complete;

@end
